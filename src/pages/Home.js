import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
/*import CourseCard from '../components/CourseCard'  -> (Courses.js) */

export default function Home() {

	return(
			<>
				<Banner/>
				<Highlights/>
				{/*<CourseCard/>  -> (Courses.js) */}
			</>

		)
}