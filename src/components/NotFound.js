import {Row, Col, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function NotFound () {

	return(

			<Row>
				<Col className="p-5">
					<h1>Page Not Found</h1>
					<p>Go back to the <a href="/">homepage</a>.</p>
				</Col>
			</Row>

		)
}