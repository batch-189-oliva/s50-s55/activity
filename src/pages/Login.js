import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Login() {

	// Allows us to consume the User context object and its properties to use for user validation
	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)


	useEffect(() => {
		if(email !== "" && password !== "" ) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	function loginUser(event) {

		event.preventDefault()


		/*
		Syntax:
			fetch('url', {options})
			.then(res => res.json())
			.then( data => {})
		*/

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {

			console.log(data)

			if (typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successsful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})

		// Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/

		/*localStorage.setItem("email", email)*/


		// Set the global user state to have properties obtained from local storage
		/*setUser({
			email: localStorage.getItem('email')
		})*/
		
		setEmail("")
		setPassword("")
		/*alert("You are now logged in.")	*/
		
	}

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then (data => {

			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})
	}

	return (

	(user.id !== null) ?
		<Navigate to="/courses" />
		
		:

		<Form className="mt-3" onSubmit={(event) => loginUser(event)}>
		<h1 classsName="text-center">Login</h1>
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
        		type="email" 
        		placeholder="Enter email"
        		value={email}
        		onChange={event => {
        			setEmail(event.target.value)
        		}} 
        		required />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password"
	        	value={password}
	        	onChange={event => {
        			setPassword(event.target.value)
        		}}  
	        	required />
	      </Form.Group>

		   {
		   	isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				Login
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
				Login
				</Button>
		   }
	      
	    </Form>


		)
}