import {Row, Col, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function Banner () {

	return(

			<Row>
				<Col className="p-5">
					<h1>Zuiit Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere.</p>
					<Button variant="primary" as={ Link } to="/courses">Enroll Now!</Button>
				</Col>
			</Row>

		)
}